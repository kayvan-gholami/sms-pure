<?php


namespace App\Repositories;


class MysqlSMSRepository
{
    public function getMessages()
    {
        // for demonstration purposes i return the products as a php array
        return [
            [
                "id" => 1,
                "phone_number" => "090000000",
                "message" => "test"
            ],
            [
                "id" => 2,
                "phone_number" => "090000000",
                "message" => "test"
            ],
            [
                "id" => 3,
                "phone_number" => "090000000",
                "message" => "test"
            ]
        ];
    }
}